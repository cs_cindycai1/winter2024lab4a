import java.util.Scanner;

public class Application {
	public static void main(String[] args) {
		java.util.Scanner reader = new java.util.Scanner(System.in);
		Student jame = new Student("2381927", "Pure & Applied Science");
		System.out.println(jame.getId());
		
		jame.setRScore(29.8);
		
		System.out.println(jame.getId());
		
		Student may = new Student("1827364", "Mechanical Engineering");
		may.setRScore(36.4);
		
		Student[] section4 = new Student[4];
		section4[0] = jame;
		section4[1] = may;
		
		section4[2] = new Student("1234567", "Studio Art");
		section4[2].setRScore(26.8);
		
		//System.out.println(section4[0].id);
		/* System.out.println(section4[2].id);
		System.out.println(section4[2].program);
		System.out.println(section4[2].rScore); */
		
		/* jame.displayScore();
		jame.printProgramLength();
		
		System.out.println("");
		
		may.displayScore();
		may.printProgramLength(); */
		
		System.out.println(section4[0].getAmountLearnt());
		System.out.println(section4[1].getAmountLearnt());
		System.out.println(section4[2].getAmountLearnt());
		
		System.out.println("Amount studied");
		int amountStudied = Integer.parseInt(reader.nextLine());
		
		section4[2].study(amountStudied);
		section4[2].study(amountStudied);
		section4[2].study(amountStudied);
		
		System.out.println(section4[0].getAmountLearnt());
		System.out.println(section4[1].getAmountLearnt());
		System.out.println(section4[2].getAmountLearnt());
		
		System.out.println(section4[0].getId());
		System.out.println(section4[0].getProgram());
		System.out.println(section4[0].getRScore());
		System.out.println(section4[0].getAmountLearnt());
		
		section4[3] = new Student("1234567", "Health Science");
		
		section4[3].setRScore(30.9);
		
		System.out.println(section4[3].getId());
		System.out.println(section4[3].getProgram());
		System.out.println(section4[3].getRScore());
		System.out.println(section4[3].getAmountLearnt());
	}
}