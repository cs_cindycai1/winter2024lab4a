public class Student {
	//fields
	private String id;
	private String program;
	private double rScore;
	private int amountLearnt;
	
	//constructor
	public Student(String id, String program) {
		this.id = id;
		this.program = program;
		this.rScore = 0.0;
		this.amountLearnt = 0;
	}
	
	//uses id to display rScore
	public void displayScore() {
		System.out.println("Your R-Score is " + this.rScore);
	}
	
	//use id to display program
	public void printProgramLength() {
		System.out.print("The duration of this program is ");
		if (this.program.equals("Mechanical Engineering")) {
			System.out.println("6 semestres");
		} else {
			System.out.println("4 semestres");
		}
	}
	
	public void study(int amountStudied) {
		this.amountLearnt += amountStudied;
	}
	
	//getter methods
	public String getId() {
		return this.id;
	}
	
	public String getProgram() {
		return this.program;
	}
	
	public double getRScore() {
		return this.rScore;
	}
	
	public int getAmountLearnt() {
		return this.amountLearnt;
	}
	
	//setters
	public void setRScore(double rScore) {
		this.rScore = rScore;
	}
}